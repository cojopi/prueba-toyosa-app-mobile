import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: LoginPage(),
  ));
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Toyosa'),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            children: [
              SizedBox(
                  height: 50
              ),
              Center(
                child: Text(
                  'LOGIN',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                  )
                )
              ),
              SizedBox(
                height: 30
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.account_box),
                  hintText: 'Username',
                  labelText: 'Username',
                ),
              ),
              SizedBox(
                  height: 20
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.password),
                  hintText: 'Password',
                  labelText: 'Password',
                ),
              ),
              SizedBox(
                  height: 20
              ),
              ElevatedButton(
                  onPressed: () {},
                  child: Text('LOGIN'),
              )
            ]
          ),
        ),
      )
    );
  }
}

